using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public bool InputDown = false;
    public float Sensitivity = 1 / 200f;
    public Vector3 InputDownPosition;
    public Vector3 CurrentPosition
    {
        get
        {
            return Input.mousePosition;
        }
    }
    public Vector3 Drag
    {
        get
        {
            if (InputDown)
                return InputDownPosition - CurrentPosition;
            else
                return Vector3.zero;
        }
    }
    [SerializeField]
    public float HorizontalAxis
    {
        get
        {
            return Drag.x;
        }
    }
    [SerializeField]
    public float VerticalAxis
    {
        get
        {
            return Drag.y;
        }
    }
    public float ClampedHorizontal
    {
        get { return Mathf.Clamp(HorizontalAxis/ Sensitivity, -1f, 1f); }
    }
    public float ClampedVertical
    {
        get { return Mathf.Clamp(VerticalAxis/ Sensitivity, -1f, 1f); }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            InputDown = true;
            InputDownPosition = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            InputDown = false;
        }
    }
}
