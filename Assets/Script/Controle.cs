using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Controle : MonoBehaviour
{
    public GameObject GO;
    private Animator anim;
    [SerializeField]
    private InputManager InputManager;
    [SerializeField]
    private Rigidbody m_Rigidbody;
    [SerializeField]
    private float m_Speed = 10f;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        IsAlive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (IsAlive)
        {
            anim.SetFloat("Speed", -InputManager.ClampedVertical);
            anim.SetFloat("Turn", -InputManager.ClampedHorizontal);
            //      anim.SetFloat("Speed", -InputManager.ClampedVertical);
            Vector3 newPosition = transform.position + (Vector3.forward * (-InputManager.ClampedVertical * m_Speed * Time.deltaTime)) + (Vector3.right * (-InputManager.ClampedHorizontal * m_Speed * Time.deltaTime));
            Debug.Log(transform.position - newPosition);
            m_Rigidbody.MovePosition(newPosition);
            if (Input.GetButtonDown("Jump"))
            {
                anim.SetTrigger("Jump");
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Equals("theive"))
        {
            Die();
        }
    }

    public bool IsAlive = true;
    public void Die()
    {
        IsAlive = false;
        StartCoroutine(DieCoroutine());
    }

    public IEnumerator DieCoroutine() 
    {
        yield return new WaitForSeconds(.3f);
        anim.SetBool("Die", true);

        yield return new WaitForSeconds(2f);
        GO.SetActive(true);


    }
}
