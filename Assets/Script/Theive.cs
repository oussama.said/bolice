using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Theive : MonoBehaviour
{
    public Transform player;
    [SerializeField]
    private NavMeshAgent agent;
    [SerializeField]
    private Animator m_Animator;
    private Vector3 m_OldPosition;
    private Vector3 m_NewPosition
    {
        get
        {
            return transform.position;
        }
    }
    private float m_Velocity
    {
        get
        {
            return (m_NewPosition - m_OldPosition).magnitude;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        m_OldPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (player != null)
        {
            agent.SetDestination(player.position);
            Vector3 distance = (player.position - transform.position).normalized;
            Quaternion newRotation = Quaternion.LookRotation(distance);
            transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, 10 * Time.deltaTime);
        }
        else
        {
            agent.isStopped = true;
        }

        m_Animator.SetBool("IdleToWalk", m_Velocity > 0.01f);
        m_OldPosition = transform.position;

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Equals("Rjab"))
        {
            m_Animator.SetBool("Kick", true);
            agent.isStopped = true;
            player = null;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.name.Equals("Rjab"))
        {
            m_Animator.SetBool("Kick", false);

        }
    }
}
