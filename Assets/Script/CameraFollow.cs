using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform PlayerTransform;
    
    private Vector3 _cameraOffset;

    [Range(0.01f, 1.0f)]
    public float smoothfactor = 0.5f;

    // Update is called once per frame
    private void Start()
    {
        _cameraOffset=transform.position - PlayerTransform.position; 
    }
    void lateUpdate()
    { 
     Vector3 newpos = PlayerTransform.position + _cameraOffset;
     transform.position = Vector3.Slerp( transform.position, newpos, smoothfactor);


    }
}
